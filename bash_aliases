# General aliases
alias ll='ls -lah'

# VIM aliases
alias vim_rc='vim ~/.vimrc'
alias bash_rc='vim ~/.bashrc'
alias bash_aliases='vim ~/.bash_aliases'

# GIT aliases
alias gitpretty='git log --oneline --graph --decorate'

# Directory aliases
alias cdprojects='cd ~/Documents/projects/'

# Bash aliases
alias source_bash='source ~/.bashrc'
