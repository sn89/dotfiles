set shiftwidth=4
set expandtab
set smarttab

set hlsearch

set foldmethod=syntax
" Toggle fold if inside a fold or regular space otherwise
nnoremap <silent> <Space> @=(foldlevel('.')?'za':"\<Space>")<CR>
